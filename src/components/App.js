import "../App.css";
import { connect } from "react-redux";
import { useState } from "react";
import Loading from "../loader/Loading";
import { SearchItem } from "../Redux/action";
import moment from "moment";
import { useEffect } from "react";
import ErrPage from "../ErrorComp/ErrPage";

function App(props) {
  const [data, setData] = useState("");
  const [loader, setLoader] = useState(false);
  const [takenData, setTakenData] = useState("");
  const [errorMsg, setErrorMsg] = useState('')

  console.log(props.myvalue);
  useEffect(() => {
    if (props.myvalue.Search_Item.success) {
      setTakenData(props.myvalue.Search_Item.data);
      setLoader(false);
      setErrorMsg("")
    }
    if(props.myvalue.Search_Item.error){
      setErrorMsg(props.myvalue.Search_Item.message.message)
      setLoader(false)
    }
  }, [props]);

  const handelchange = (event) => {
    setData(event.target.value);
  };

  const handelsubmit = (event) => {
    event.preventDefault();
    props.SearchItem(data);
    setData("");
  };

  return (
    <>
      <div className="topnav">
        <span>HackersNews</span>
        <div className="search-container">
          <form onSubmit={handelsubmit}>
            <input
              type="text"
              placeholder="Search"
              name="search"
              onChange={handelchange}
              value={data}
              autoComplete="off"
            />
            <button onClick={() => setLoader(true, setTakenData(""))}>
              Search
            </button>
          </form>
        </div>
      </div>
      <div className="rock">
        {/* {errorMsg ?<> <h1>{errorMsg}</h1> <h2>Check for URL you entered</h2></>
        : (
          <></>
        )} */}
        {errorMsg && <ErrPage msg={errorMsg}/>}
        {loader && <Loading />}
        {takenData ? (
          takenData.map((item) => {
            return (
              <>
                <p>
                  <b>{item.title}</b>{" "}
                  <a
                    className="link1"
                    href={item.url}
                    target="_blank"
                    rel="noreferrer"
                  >
                    ({item.url})
                  </a>
                </p>
                <p>
                  {item.points + " points"} | {item.author} | posted at{" "}
                  {moment(item.created_at).format("D/MM/YYYY")} |{" "}
                  {item.num_comments + " comments"}
                </p>
                <hr />
              </>
            );
          })
        ) : (
          <></>
        )}
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    myvalue: state.values,
  };
};
const mapDispatchToProps = (dispatch) => ({
  SearchItem: (e) => dispatch(SearchItem(e)),
});
export default connect(mapStateToProps, mapDispatchToProps)(App);
