import React from "react"
import "./loader.css"

const Loading =()=>{

    return (
      <>
        <div class="loadingio-spinner-gear-mugfj141ehr">
          <div class="ldio-jz7b09nmpt">
            <div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
            </div>
          </div>
        </div>
      </>
    );
}

export default Loading;