import axios from "axios";

export const Search_Item_Request = () => ({
  type: "SEARCH_ITEM_REQUEST"
});

export const Search_Item_Success = (data) => ({
  type: "SEARCH_ITEM_SUCCESS",
  data,
});

export const Search_Item_Failed = (message) =>({
  type: "SEARCH_ITEM_FAILED",
  message,
});

export const SearchItem = (data) => {
  return (dispatch) => {
    try {
      dispatch(Search_Item_Request());
      const url = "https://hn.algolia.com/api/v1/search?query=";
      const res = axios.get(url + data);
      res
        .then((res) => {
          if (res.status === 200) {
            dispatch(Search_Item_Success(res.data.hits));
          }
        })
        .catch((err) => {
          dispatch(Search_Item_Failed(err));
        });
    } catch (error) {
      console.log(error);
    }
  };
};


// export const Search_Item_Success = (data) => ({
//   type: "SEARCH_ITEM_SUCCESS",
//   data,
// });

// export const SearchItem = (data) => {
//   return (dispatch) => {
//     try {
//       const url = "https://hn.algolia.com/api/v1/search?query=";
//       const res = axios.get(url + data);
//       res
//         .then((res) => {
//           if (res.status === 200) {
//             dispatch(Search_Item_Success(res.data.hits));
//           }
//         })
//     } catch (error) {
//       console.log(error);
//     }
//   };
// };

