
const initial_value = {
  Search_Item: {
    loading: false,
    success: false,
    error: false,
  },
};

const Reducer = (state = initial_value, action) => {
  switch (action.type) {
    case "SEARCH_ITEM_REQUEST":
      return {
        ...state,
        Search_Item: {
          loading: true,
          success: false,
          error: false,
        },
      };

    case "SEARCH_ITEM_SUCCESS":
      return {
        ...state,
        Search_Item: {
          loading: false,
          success: true,
          error: false,
          data: action.data,
        },
      };

    case "SEARCH_ITEM_FAILED":
      console.log(action.message);
      return {
        ...state,
        Search_Item: {
          loading: false,
          success:false,
          error: true,
          message: action.message,
        },
      };
    default:
      return state;
  }
};
export default Reducer;

// const initial_value = {
//   Search_Item: {
//     success: false,
//   }
// };

// const Reducer = (state = initial_value, action) => {
//   switch (action.type) {
//     case "SEARCH_ITEM_SUCCESS":
//        return {
//         ...state,
//         Search_Item: {
//           success: true,
//           data: action.data,
//         },
//       };

//     default:
//       return state;
//   }
// };
// export default Reducer;

