import React from "react";
import "./style.css";

const ErrPage = (props) => {
  return (
    <>
      <div id="notfound">
        <div className="notfound">
          <div className="notfound-404">
            <h1>Oops!</h1>
            <h2>{props.msg}</h2>
          </div>
        </div>
      </div>
    </>
  );
};

export default ErrPage;
